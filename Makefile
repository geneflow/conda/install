install-miniconda:
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	sh ./Miniconda3-latest-Linux-x86_64.sh -b -u
	PATH=~/miniconda3/bin:${PATH} conda init bash
	PATH=~/miniconda3/bin:${PATH} conda config --set auto_activate_base false
	rm ./Miniconda3-latest-Linux-x86_64.sh
