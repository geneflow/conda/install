Conda Environment Installation Scripts
======================================

This is repo includes a Makefile for installing a local copy of Conda.

Instructions
------------

Clone the conda environment installation repository, enter the directory, and make:

.. code-block:: bash

    git clone https://gitlab.com/geneflow/conda/install.git
    cd install
    make install-miniconda
    source ~/.bashrc
